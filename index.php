<?php
// Show all errors (for educational purposes)
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

// Constanten (connectie-instellingen databank)
define('DB_HOST', 'localhost');
define('DB_USER', 'Tristan_Smismans');
define('DB_PASS', 'M&0o73yf8');
define('DB_NAME', 'OpenAuthentication');

date_default_timezone_set('Europe/Brussels');

// Verbinding maken met de databank
try {
    $db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASS);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Verbindingsfout: ' . $e->getMessage();
    exit;
}

$id_google = isset($_POST['userId']) ? (string)$_POST['userId'] : '';
$naam = isset($_POST['fullName']) ? (string)$_POST['fullName'] : '';
$voornaam = isset($_POST['givenName']) ? (string)$_POST['givenName'] : '';
$familienaam = isset($_POST['familyName']) ? (string)$_POST['familyName'] : '';
$email = isset($_POST['email']) ? (string)$_POST['email'] : '';
$profiel = isset($_POST['profilePictureImg']) ? (string)$_POST['profilePictureImg'] : '';
$land = isset($_POST['countrySelect']) ? (string)$_POST['countrySelect'] : '';

// form is sent: perform formchecking!
if (isset($_POST['saveButton'])) {
	$id=0;
	// build & execute prepared statement
    $stmt = $db->prepare("INSERT INTO Info (id, id_Google, Naam, Voornaam, Familienaam, Email-Adres, Profielfoto, Land)
	VALUES(?,?,?,?,?,?,?,?)");
	$stmt->bindParam(1, $id);
	$stmt->bindParam(2, $id_google);
	$stmt->bindParam(3, $naam);
	$stmt->bindParam(4, $voornaam);
	$stmt->bindParam(5, $familienaam);
	$stmt->bindParam(6, $email);
	$stmt->bindParam(7, $profiel);
	$stmt->bindParam(8, $land);
    $stmt->execute(array($id, $id_google, $naam, $voornaam, $familienaam, $email, $profiel, $land));
	if ($stmt->rowCount() === 1) {
		  echo "De gegevens zijn succesvol toegevoegd aan de database.";
	} else {
		  echo "Er is een fout opgetreden bij het toevoegen van de gegevens.";
	}
}
?>

<!DOCTYPE html>
<html>
  <html lang="en">
	  <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="google-signin-client_id" content="1066385187218-dhnpgdi08c31lqdokmdutmn3fdl5tutf.apps.googleusercontent.com">
        <title>Loginpagina open authentication</title>
        <script src="https://accounts.google.com/gsi/client" async=""></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
	  </head>
    <body>
		<main>
		  <section class="vh-100">
			  <div class="container py-5 h-100">
					<div class="row d-flex align-items-center justify-content-center h-100">
						<div class="col-md-8 col-lg-7 col-xl-6">
							<img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
								class="img-fluid" alt="Phone image">
						</div>
						<div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
							<div class="form-outline mb-4">
								<input type="email" id="form1Example13" class="form-control form-control-lg" />
								<label class="form-label" for="form1Example13">Email address</label>
							</div>
							
							<div class="form-outline mb-4">
								<input type="password" id="form1Example23" class="form-control form-control-lg" />
								<label class="form-label" for="form1Example23">Password</label>
							</div>
							
							<div class="d-flex justify-content-around align-items-center mb-4">
								<!-- Checkbox -->
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
									<label class="form-check-label" for="form1Example3"> Remember me </label>
								</div>
								<a href="">Forgot password?</a>
							</div>
							
							<button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>

							<div class="divider d-flex align-items-center my-4">
								<p class="text-center fw-bold mx-3 mb-0 text-muted">OR</p>
							</div>

							<div id="g_id_onload"
								data-client_id="1066385187218-dhnpgdi08c31lqdokmdutmn3fdl5tutf.apps.googleusercontent.com"
								data-context="signin"
								data-ux_mode="popup"
								data-callback="handleCredentialResponse">
							</div>

							<div class="g_id_signin"
								data-type="standard"
								data-shape="rectangular"
								data-theme="outline"
								data-text="signin_with"
								data-size="large"
								data-locale="nl"
								data-logo_alignment="center"
								data-width="200">
							</div>

							<div class="loginInfo">
								<p id="userId" class="bold-text">ID : </p>
								<p id="fullName" class="bold-text">Volledige naam : </p>
								<p id="givenName" class="bold-text">Gegeven naam : </p>
								<p id="familyName" class="bold-text">Familienaam : </p>
								<p id="email" class="bold-text">Email : </p>
								<p class="bold-text">Profielfoto : <img src="" id="profilePictureImg" alt="Profielfoto"></p>
								<p class="bold-text">Gelieve uw geboorteland te selecteren:</p>
								<select id="countrySelect" onchange="updateFlag()">
									<option value="Nederland">Nederland</option>
									<option value="België">België</option>
									<option value="Duitsland">Duitsland</option>
									<option value="Frankrijk">Frankrijk</option>
									<option value="Verenigd Koninkrijk">Verenigd Koninkrijk</option>
									<option value="Italië">Italië</option>
									<option value="Spanje">Spanje</option>
									<option value="Portugal">Portugal</option>
									<option value="Zwitserland">Zwitserland</option>
									<option value="Oostenrijk">Oostenrijk</option>
									<option value="Denemarken">Denemarken</option>
									<option value="Zweden">Zweden</option>
									<option value="Noorwegen">Noorwegen</option>
									<option value="Finland">Finland</option>
									<option value="Ierland">Ierland</option>
									<option value="Griekenland">Griekenland</option>
									<option value="Polen">Polen</option>
									<option value="Tsjechië">Tsjechië</option>
									<option value="Hongarije">Hongarije</option>
									<option value="Roemenië">Roemenië</option>
									<option value="Bulgarije">Bulgarije</option>
									<option value="Slovenië">Slovenië</option>
									<option value="Slowakije">Slowakije</option>
									<option value="Kroatië">Kroatië</option>
									<option value="Estland">Estland</option>
									<option value="Letland">Letland</option>
									<option value="Litouwen">Litouwen</option>
									<option value="Luxemburg">Luxemburg</option>
									<option value="Malta">Malta</option>
									<option value="Cyprus">Cyprus</option>
									<option value="IJsland">IJsland</option>
									<option value="Verenigde Staten">Verenigde Staten</option>
									<option value="Canada">Canada</option>
									<option value="Australië">Australië</option>
									<option value="Japan">Japan</option>
								</select>
								<img id="flagImage" class ="flagImage" src="" alt="Vlag">
								<p class="distanceText" id="distanceText"></p>
							</div>
							<button class="saveButton" onclick="saveCountry()">Opslaan</button>

							<script>
								let flags = {
									"Nederland": "https://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_the_Netherlands.svg",
									"België": "https://upload.wikimedia.org/wikipedia/commons/9/92/Flag_of_Belgium_%28civil%29.svg",
									"Duitsland": "https://upload.wikimedia.org/wikipedia/commons/b/ba/Flag_of_Germany.svg",
									"Frankrijk": "https://upload.wikimedia.org/wikipedia/commons/c/c3/Flag_of_France.svg",
									"Verenigd Koninkrijk": "https://upload.wikimedia.org/wikipedia/commons/a/ae/Flag_of_the_United_Kingdom.svg",
									"Italië": "https://upload.wikimedia.org/wikipedia/commons/0/03/Flag_of_Italy.svg",
									"Spanje": "https://upload.wikimedia.org/wikipedia/commons/9/9a/Flag_of_Spain.svg",
									"Portugal": "https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Portugal.svg",
									"Zwitserland": "https://upload.wikimedia.org/wikipedia/commons/0/08/Flag_of_Switzerland_%28Pantone%29.svg",
									"Oostenrijk": "https://upload.wikimedia.org/wikipedia/commons/4/41/Flag_of_Austria.svg",
									"Denemarken": "https://upload.wikimedia.org/wikipedia/commons/9/9c/Flag_of_Denmark.svg",
									"Zweden": "https://upload.wikimedia.org/wikipedia/en/4/4c/Flag_of_Sweden.svg",
									"Noorwegen": "https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Norway.svg",
									"Finland": "https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_of_Finland.svg",
									"Ierland": "https://upload.wikimedia.org/wikipedia/commons/4/45/Flag_of_Ireland.svg",
									"Griekenland": "https://upload.wikimedia.org/wikipedia/commons/5/5c/Flag_of_Greece.svg",
									"Polen": "https://upload.wikimedia.org/wikipedia/commons/1/12/Flag_of_Poland.svg",
									"Tsjechië": "https://upload.wikimedia.org/wikipedia/commons/c/cb/Flag_of_the_Czech_Republic.svg",
									"Hongarije": "https://upload.wikimedia.org/wikipedia/commons/c/c1/Flag_of_Hungary.svg",
									"Roemenië": "https://upload.wikimedia.org/wikipedia/commons/7/73/Flag_of_Romania.svg",
									"Bulgarije": "https://upload.wikimedia.org/wikipedia/commons/9/9a/Flag_of_Bulgaria.svg",
									"Slovenië": "https://upload.wikimedia.org/wikipedia/commons/f/f0/Flag_of_Slovenia.svg",
									"Slowakije": "https://upload.wikimedia.org/wikipedia/commons/e/e6/Flag_of_Slovakia.svg",
									"Kroatië": "https://upload.wikimedia.org/wikipedia/commons/1/1b/Flag_of_Croatia.svg",
									"Estland": "https://upload.wikimedia.org/wikipedia/commons/8/8f/Flag_of_Estonia.svg",
									"Letland": "https://upload.wikimedia.org/wikipedia/commons/8/84/Flag_of_Latvia.svg",
									"Litouwen": "https://upload.wikimedia.org/wikipedia/commons/1/11/Flag_of_Lithuania.svg",
									"Luxemburg": "https://upload.wikimedia.org/wikipedia/commons/d/da/Flag_of_Luxembourg.svg",
									"Malta": "https://upload.wikimedia.org/wikipedia/commons/7/73/Flag_of_Malta.svg",
									"Cyprus": "https://upload.wikimedia.org/wikipedia/commons/d/d4/Flag_of_Cyprus.svg",
									"IJsland": "https://upload.wikimedia.org/wikipedia/commons/c/ce/Flag_of_Iceland.svg",
									"Verenigde Staten": "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
									"Canada": "https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Canada_%28Pantone%29.svg",
									"Australië": "https://upload.wikimedia.org/wikipedia/commons/b/b9/Flag_of_Australia.svg",
									"Japan": "https://upload.wikimedia.org/wikipedia/en/9/9e/Flag_of_Japan.svg",
								};

								document.getElementById("countrySelect").selectedIndex = 1;
								function updateFlag() {
									let countrySelect = document.getElementById("countrySelect");
									let selectedCountry = countrySelect.options[countrySelect.selectedIndex].value;
									let flagImage = document.getElementById("flagImage");
									flagImage.src = flags[selectedCountry];
									let countryCoords = countryCoordinates[selectedCountry];
									let distance = calculateDistance(gentCordinaten.latitude, gentCordinaten.longitude, countryCoords.latitude,
																	 countryCoords.longitude);
									document.getElementById("distanceText").textContent = "Afstand van Gent tot de hoofdstad van uw geboorteland: " + distance + " km";
								}

								let gentCordinaten = {
									latitude: 51.0543,
									longitude: 3.7174
								};

								let countryCoordinates = {
									"Nederland": { latitude: 52.3676, longitude: 4.9041 },
									"België": { latitude: 50.8503, longitude: 4.3517 },
									"Duitsland": { latitude: 52.5200, longitude: 13.4050 },
									"Frankrijk": { latitude: 48.8566, longitude: 2.3522 },
									"Verenigd Koninkrijk": { latitude: 51.5074, longitude: -0.1278 },
									"Italië": { latitude: 41.9028, longitude: 12.4964 },
									"Spanje": { latitude: 40.4168, longitude: -3.7038 },
									"Portugal": { latitude: 38.7223, longitude: -9.1393 },
									"Zwitserland": { latitude: 47.3769, longitude: 8.5417 },
									"Oostenrijk": { latitude: 48.2082, longitude: 16.3738 },
									"Denemarken": { latitude: 55.6761, longitude: 12.5683 },
									"Zweden": { latitude: 59.3293, longitude: 18.0686 },
									"Noorwegen": { latitude: 59.9139, longitude: 10.7522 },
									"Finland": { latitude: 60.1695, longitude: 24.9354 },
									"Ierland": { latitude: 53.3498, longitude: -6.2603 },
									"Griekenland": { latitude: 37.9838, longitude: 23.7275 },
									"Polen": { latitude: 52.2297, longitude: 21.0122 },
									"Tsjechië": { latitude: 50.0755, longitude: 14.4378 },
									"Hongarije": { latitude: 47.4979, longitude: 19.0402 },
									"Roemenië": { latitude: 44.4268, longitude: 26.1025 },
									"Bulgarije": { latitude: 42.6977, longitude: 23.3219 },
									"Slovenië": { latitude: 46.0569, longitude: 14.5058 },
									"Slowakije": { latitude: 48.1486, longitude: 17.1077 },
									"Kroatië": { latitude: 45.8150, longitude: 15.9819 },
									"Estland": { latitude: 59.4370, longitude: 24.7536 },
									"Letland": { latitude: 56.9496, longitude: 24.1052 },
									"Litouwen": { latitude: 54.6872, longitude: 25.2797 },
									"Luxemburg": { latitude: 49.6116, longitude: 6.1319 },
									"Malta": { latitude: 35.8989, longitude: 14.5146 },
									"Cyprus": { latitude: 35.1676, longitude: 33.3736 },
									"IJsland": { latitude: 64.1466, longitude: -21.9426 },
									"Verenigde Staten": { latitude: 38.8951, longitude: -77.0364 },
									"Canada": { latitude: 45.4215, longitude: -75.6972 },
									"Australië": { latitude: -35.2820, longitude: 149.1287 },
									"Japan": { latitude: 35.6895, longitude: 139.6917 }
								};

								function calculateDistance(lat1, lon1, lat2, lon2) {
									let R = 6371;
									let dLat = deg2rad(lat2 - lat1);
									let dLon = deg2rad(lon2 - lon1);
									let a =
										Math.sin(dLat / 2) * Math.sin(dLat / 2) +
										Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
										Math.sin(dLon / 2) * Math.sin(dLon / 2);
									let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
									let distance = R * c;
									return distance.toFixed(2); 
								}

								function deg2rad(deg) {
									return deg * (Math.PI / 180);
								}

								function decodeJwtResponse(token) {
									let baseUrl = token.split('.')[1];
									let base = baseUrl.replace(/-/g, '+').replace(/_/g, '/');
									let jsonPayload = decodeURIComponent(window.atob(base).split('').map(function(c) {
										return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
									}).join(''));

									return JSON.parse(jsonPayload);
								}

								let responsePayload;

								function handleCredentialResponse(response) {
									responsePayload = decodeJwtResponse(response.credential);

									document.getElementById('userId').textContent = "ID: " + responsePayload.sub;
									document.getElementById('fullName').textContent = "Volledige naam: " + responsePayload.name;
									document.getElementById('givenName').textContent = "Gegeven naam: " + responsePayload.given_name;
									document.getElementById('familyName').textContent = "Familienaam: " + responsePayload.family_name;
									document.getElementById('email').textContent = "Email: " + responsePayload.email;

									let profilePictureImg = document.getElementById("profilePictureImg");
									profilePictureImg.src = responsePayload.picture;
									profilePictureImg.alt = "Profielfoto van " + responsePayload.name;

									let country = document.getElementById("countrySelect").value;

									$.ajax({
										type: "POST",
										url: 'index.php',
										data: {
											userId: responsePayload.sub,
											fullName: responsePayload.name,
											givenName: responsePayload.given_name,
											familyName: responsePayload.family_name,
											email: responsePayload.email,
											profilePicture: responsePayload.picture,
											country: country
										},
										success: function (data) {
											if (data === "Error") {
												$("#error").show();
											} else {
												document.getElementById("countrySelect").value = data;
												updateFlag();
											}
										},

										error: function(xhr, status, error) {
											console.error(xhr.responseText);
											console.error(status, error);
											$("#error").show();
										}
									});
								}


								function saveCountry() {
									let countrySelect = document.getElementById("countrySelect");
									let selectedCountry = countrySelect.options[countrySelect.selectedIndex].value;
									if (!selectedCountry) {
										window.alert("Gelieve een land te selecteren.");
										return;
									}
									let userId = responsePayload.sub;
									let country = selectedCountry;

									$.ajax({
										type: "POST",
										url: 'index.php',
										data: {
											userId: userId,
											country: country
										},
										success: function (data) {
											if (data === "Error") {
												$("#error").show(); 
											} else {
												window.alert("Uw geboorteLand is opgeslagen!");
											}
										},
										error: function(xhr, status, error) {
											console.error(xhr.responseText);
											console.error(status, error);
											$("#error").show(); 
										}
									});
								}

							</script>
							
						</div>
					  </div>
				  </div>
			  </section>
		  </main>
		<link href="login.css" rel="stylesheet" />
		<script type="module" src="script.js"></script>
  </body>
</html>