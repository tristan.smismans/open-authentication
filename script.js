;(function () {
    'use strict';
	document.addEventListener('DOMContentLoaded', function () {
        let inputElements = document.querySelectorAll('.form-control');
        inputElements.forEach(function (inputElement) {
            let labelElement = inputElement.nextElementSibling;

            inputElement.addEventListener('input', function () {
                let inputValue = inputElement.value;
                if (inputValue) {
                    labelElement.classList.add('hidden');
                } else {
                    labelElement.classList.remove('hidden');
                }
            });
        });
    });
})();